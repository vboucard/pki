# Installation de step

```bash
wget https://github.com/smallstep/cli/releases/download/v0.15.16/step_linux_0.15.16_amd64.tar.gz
tar xvzf step_linux_0.15.16_amd64.tar.gz
chmod +x step
mv step /usr/local/bin
```

# Démarrer le conteneur Docker

## Création du volume

```bash
docker volume create step
```

## Premier démarrage du conteneur

```bash
docker run -it -v step:/home/step smallstep/step-ca sh

$ step ca init
✔ What would you like to name your new PKI?: Smallstep
✔ What DNS names or IP addresses would you like to add to your new CA? localhost
✔ What address will your new CA listen at?: :9000
✔ What would you like to name the first provisioner for your new CA?: admin
✔ What do you want your password to be?: <your password here>
 
Generating root certificate...
all done!
 
Generating intermediate certificate...
all done!
 
✔ Root certificate: /home/step/certs/root_ca.crt
✔ Root private key: /home/step/secrets/root_ca_key
✔ Root fingerprint: f9e45ae9ec5d42d702ce39fd9f3125372ce54d0b29a5ff3016b31d9b887a61a4
✔ Intermediate certificate: /home/step/certs/intermediate_ca.crt
✔ Intermediate private key: /home/step/secrets/intermediate_ca_key
✔ Default configuration: /home/step/config/defaults.json
✔ Certificate Authority configuration: /home/step/config/ca.json
 
Your PKI is ready to go. To generate certificates for individual services see 'step help ca'

echo <your password here> > /home/step/secrets/password
```

## Utiliser le conteneur

```bash
docker run -d -p 127.0.0.1:9000:9000 -v step:/home/step smallstep/step-ca
```

## Installer le certificat

```bash
step ca bootstrap --ca-url https://localhost:9000 --install --fingerprint <your fingerprint>
```

# Transformer notre PKI en Autorité de certification intermédiaire

## Configuration initiale

A l'initialisation de notre PKI, plusieurs fichiers sont créés dans `~/.step/` dont :

- `~/.step/certs/root_ca.crt` the CA certificate

- `~/.step/secrets/root_ca_key` the CA signing key

- `~/.step/certs/intermediate_ca.crt` the intermediate CA cert

- `~/.step/secrets/intermediate_ca_key` the intermediate signing key used by step-ca

```bash
rm ~/.step/secrets/root_ca_key
```

## Générer  une clé et une demande de certificat intermédiaire

Remplacer le certificat racine de step par celui de votre PKI

```bash
$ mv  </path/to/your/existing/root.crt> ~/.step/certs/root_ca.crt
```

Générer la nouvelle clé privée et la demande de certificat intermédiaire

```bash
step certificate create "Intermediate CA Name" intermediate.csr intermediate_ca_key --csr
```

## Utiliser le Certificate Signing Request sur l'AD

```powershell
certreq -submit -attrib "CertificateTemplate:SubCA" intermediate.csr intermediate.crt
```

## Remplacer les fichiers

```bash
$ mv intermediate.crt ~/.step/certs/intermediate_ca.crt
$ mv intermediate_ca_key ~/.step/secrets/intermediate_ca_key
```




















